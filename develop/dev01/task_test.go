package main

import (
	"testing"
	"time"
)

// Тест проверяет, что значения полученные в функции getTime и time.Now равные, с погрешностью в секунду
func TestGetTime(t *testing.T) {
	cur := getTime()
	want := time.Now()

	if want.Sub(cur).Abs().Round(time.Second) > time.Second*0 {
		t.Errorf("not equal func: %v and want: %v", cur, want)
		t.Fail()
	}
}
