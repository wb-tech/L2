package main

import (
	"fmt"
	"net/url"
	"testing"
)

func TestLinkNode_Search(t *testing.T) {
	URL, err := url.Parse("http://google.com")
	if err != nil {
		t.Error(err)
	}
	links := NewLinkNode(2, *URL)
	if err := links.Search(); err != nil {
		t.Error(err)
	}
	fmt.Println(links)
}

func TestLinkNode_Links(t *testing.T) {
	URL, err := url.Parse("http://google.com")
	if err != nil {
		t.Error(err)
	}
	links := NewLinkNode(2, *URL)
	if err := links.Search(); err != nil {
		t.Error(err)
	}

	fmt.Println(links)

	fmt.Println()

	for _, ln := range links.Links() {
		fmt.Println(ln)
	}

	fmt.Println(len(links.Links()))
}
