package main

import (
	"flag"
	"fmt"
	"log"
	"net/url"
)

/*
=== Утилита wget ===

Реализовать утилиту wget с возможностью скачивать сайты целиком

Программа должна проходить все тесты. Код должен проходить проверки go vet и golint.
*/

//TODO: Надо обрабатывать некорректные ответы
//TODO: Скачивать css и js

var (
	rawUrl    string
	level     = flag.Int("l", 5, "level of depth for download")
	recursive = flag.Bool("r", false, "recursively download page")
	usage     = func() {
		fmt.Println("Program for downloading cites")
		flag.PrintDefaults()
	}
)

func main() {
	flag.Parse()
	flag.Usage = usage

	if flag.NArg() < 1 {
		log.Fatal("not found cite url")
	}

	rawUrl = flag.Arg(0)

	URL, err := url.Parse(rawUrl)
	if err != nil {
		log.Fatal(err)
	}

	var downloader Downloader

	if !*recursive {
		downloader = NewSingleDownloader()
	} else {
		downloader = NewRecursiveDownloader(*level)
	}

	if err = downloader.Download(*URL); err != nil {
		log.Fatal(err)
	}
}
