package main

import (
	"bufio"
	"io"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"
)

type Downloader interface {
	Download(URL url.URL) error
}

type SingleDownloader struct{}

func NewSingleDownloader() SingleDownloader {
	return SingleDownloader{}
}

func (SingleDownloader) Download(URL url.URL) error {
	resp, err := http.Get(URL.String())
	if err != nil {
		return err
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	file, err := os.OpenFile("./index.html", os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		return err
	}

	writer := bufio.NewWriter(file)

	if _, err = writer.Write(body); err != nil {
		return err
	}

	return nil
}

type RecursiveDownloader struct {
	level int
}

func NewRecursiveDownloader(lvl int) RecursiveDownloader {
	return RecursiveDownloader{lvl}
}

// TODO: Для иерархии сделать ссылки в виде дерева
func (dwn RecursiveDownloader) Download(URL url.URL) error {
	links := NewLinkNode(dwn.level, URL)

	if err := links.Search(); err != nil {
		return err
	}

	if err := dwn.download(links.Links()); err != nil {
		return err
	}

	return nil
}

func (dwn RecursiveDownloader) download(links []url.URL) error {
	for _, link := range links {
		path := urlToOS(link)
		if err := os.MkdirAll(path, 0777); err != nil {
			return err
		}

		path = filepath.Join(path, "index.html")

		fl, err := os.Create(path)
		if err != nil {
			return err
		}
		defer fl.Close()

		resp, err := http.Get(link.String())
		if err != nil {
			return err
		}

		bytes, err := io.ReadAll(resp.Body)
		if err != nil {
			return err
		}

		if _, err := fl.Write(bytes); err != nil {
			return err
		}
	}
	return nil
}

func urlToOS(url2 url.URL) string {
	build := strings.Builder{}
	build.WriteString("./")
	build.WriteString(url2.Hostname())
	build.WriteString("/")
	build.WriteString(url2.Path)
	return build.String()
}
