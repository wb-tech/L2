package main

import (
	"golang.org/x/net/html"
	"net/http"
	"net/url"
)

type LinkNode struct {
	level  int
	link   url.URL
	nested []*LinkNode
}

func NewLinkNode(level int, link url.URL) *LinkNode {
	return &LinkNode{link: link, level: level}
}

func (ln *LinkNode) Search() error {
	if ln.level == 0 {
		return nil
	}

	resp, err := http.Get(ln.link.String())
	if err != nil {
		return err
	}

	nd, err := html.Parse(resp.Body)
	if err != nil {
		return err
	}

	var f func(n *html.Node) error
	f = func(n *html.Node) error {
		if n.Type == html.ElementNode && n.Data == "a" {
			for _, a := range n.Attr {
				if a.Key == "href" {
					url2, err := url.Parse(a.Val)
					if err != nil {
						continue
					}

					if url2.IsAbs() {
						if url2.Hostname() != ln.link.Hostname() {
							continue
						}

						nln := NewLinkNode(ln.level-1, *url2)
						if err = nln.Search(); err != nil {
							return err
						}
						ln.nested = append(ln.nested, nln)
					} else {
						url2 = ln.link.JoinPath(url2.String())
						nln := NewLinkNode(ln.level-1, *url2)
						if err = nln.Search(); err != nil {
							return err
						}
						ln.nested = append(ln.nested, nln)
					}
					break
				}
			}
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			if err = f(c); err != nil {
				return err
			}
		}

		return nil
	}
	if err = f(nd); err != nil {
		return err
	}

	return nil
}

func (ln *LinkNode) Links() []url.URL {
	res := []url.URL{ln.link}

	for _, link := range ln.nested {
		res = append(res, link.Links()...)
	}

	return res
}
