package main

import (
	"github.com/spf13/viper"
	"log/slog"
)

func InitConfig(path string) *Config {
	config := &Config{
		"8080",
		"info",
	}

	viper.SetConfigType("env")
	viper.SetConfigFile(path)
	if err := viper.ReadInConfig(); err != nil {
		slog.Warn("Config not read:", "error", err.Error())
		config.DefaultConfigLog()
		return config
	}

	for k, v := range viper.AllSettings() {
		slog.Info("Setting:", "Key", k, "Value", v)
	}

	config.port = viper.GetString("PORT")
	config.logLevel = viper.GetString("LOG_LEVEL")

	return config
}

type Config struct {
	port     string
	logLevel string
}

func (conf *Config) GetPort() string {
	return conf.port
}

func (conf *Config) GetLogLevel() slog.Level {
	switch conf.logLevel {
	case "Error":
		return slog.LevelError
	case "Warn":
		return slog.LevelWarn
	case "Info":
		return slog.LevelInfo
	case "Debug":
		return slog.LevelDebug
	default:
		return slog.LevelInfo
	}
}

func (conf *Config) DefaultConfigLog() {
	slog.Info("Setting:", "Key", "PORT", "Value", conf.port)
	slog.Info("Setting:", "Key", "LOG_LEVEL", "Value", conf.logLevel)
}
