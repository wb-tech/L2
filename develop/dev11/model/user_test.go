package model

import (
	"encoding/json"
	"fmt"
	"testing"
	"time"
)

var timee, _ = time.Parse(time.DateTime, "2024-02-14 10:00:00")

func TestUserAddToEmptyEvent(t *testing.T) {
	user := NewUser(1)
	eventTime := timee.Format(time.DateTime)
	eventDesc := "a"
	expected := "a"

	if err := user.AddEvent(eventTime, eventDesc); err != nil {
		t.Errorf("Cannot add event with error: %v", err)
		return
	}

	list, err := user.EventsByDate(timee.Format(time.DateOnly))
	if err != nil {
		t.Errorf("Cannot get event by time with error: %v", err)
		return
	}

	if expected != list[timee.Format(time.DateOnly)].String() {
		t.Errorf("Not equal exp and event: %v - %v", expected, list[timee.Format(time.TimeOnly)].String())
	}
}

func TestUserAddEvent(t *testing.T) {
	newEventList := &EventList{1, eventWithoutError(timee.Add(time.Minute*5).Format(time.TimeOnly), "a")}
	user := &User{1, map[string]*EventList{
		timee.Format(time.DateOnly): newEventList,
	}}
	eventTime := timee.Format(time.DateTime)
	eventDesc := "b"
	expected := "b a"

	if err := user.AddEvent(eventTime, eventDesc); err != nil {
		t.Errorf("Cannot add event with error: %v", err)
		return
	}

	list, err := user.EventsByDate(timee.Format(time.DateOnly))
	if err != nil {
		t.Errorf("Cannot get event by time with error: %v", err)
		return
	}

	if expected != list[timee.Format(time.DateOnly)].String() {
		t.Errorf("Not equal exp and event: %v - %v", expected, list[timee.Format(time.TimeOnly)].String())
		return
	}
}

func TestUserUpdateNotExist(t *testing.T) {
	user := NewUser(1)
	eventTime := timee.Format(time.DateTime)
	eventDesc := "a"

	if user.UpdateEvent(eventTime, eventDesc) == nil {
		t.Errorf("update not exist event in useer impossible")
		return
	}
}

func TestUserUpdateEvent(t *testing.T) {
	newEventList := &EventList{1, eventWithoutError(timee.Format(time.TimeOnly), "a")}

	user := &User{1, map[string]*EventList{
		timee.Format(time.DateOnly): newEventList,
	}}
	eventTime := timee.Format(time.DateTime)
	eventDesc := "b"
	expected := "b"

	if err := user.UpdateEvent(eventTime, eventDesc); err != nil {
		t.Errorf("cannot update event with error: %v", err)
		return
	}

	list, err := user.EventsByDate(timee.Format(time.DateOnly))
	if err != nil {
		t.Errorf("cannot get event by time with error: %v", err)
		return
	}

	if expected != list[timee.Format(time.DateOnly)].String() {
		t.Errorf("not equal exp and event: %v - %v", expected, list[timee.Format(time.TimeOnly)].String())
		return
	}
}

func TestUserDeleteNotExist(t *testing.T) {
	user := NewUser(1)
	eventTime := timee.Format(time.DateTime)

	if user.DeleteEvent(eventTime) == nil {
		t.Errorf("delete not exist event in useer impossible")
		return
	}
}

func TestUserDeleteEvent(t *testing.T) {
	newEventList := &EventList{1, eventWithoutError(timee.Format(time.TimeOnly), "a")}

	user := &User{1, map[string]*EventList{
		timee.Format(time.DateOnly): newEventList,
	}}
	eventTime := timee.Format(time.DateTime)

	if err := user.DeleteEvent(eventTime); err != nil {
		t.Errorf("cannot delete event with error: %v", err)
		return
	}

	list, err := user.EventsByDate(timee.Format(time.DateOnly))
	if err != nil {
		t.Errorf("cannot get event by time with error: %v", err)
		return
	}

	if _, ok := list[timee.Format(time.DateOnly)]; ok {
		t.Errorf("not deleted once eventlist")
		return
	}
}

func TestUserMarshal(t *testing.T) {
	user := NewUser(1)
	user.AddEvent(time.Now().Format(time.DateTime), "a")
	user.AddEvent(time.Now().Add(time.Minute).Format(time.DateTime), "b")
	user.AddEvent(time.Now().Add(time.Minute*2).Format(time.DateTime), "c")
	user.AddEvent(time.Now().Add(time.Hour*48).Format(time.DateTime), "a")
	user.AddEvent(time.Now().Add(time.Hour*48+time.Minute).Format(time.DateTime), "b")
	user.AddEvent(time.Now().Add(time.Hour*48+time.Minute*2).Format(time.DateTime), "c")
	str, _ := json.MarshalIndent(user, "", "  ")
	fmt.Println(string(str))
}
