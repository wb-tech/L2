package model

import (
	"encoding/json"
	"strings"
	"time"
)

type EventList struct {
	len  int
	head *Event
}

func NewEventList() *EventList {
	return &EventList{0, nil}
}

func (el *EventList) Add(timee, desc string) bool {
	event, err := NewEvent(timee, desc)
	if err != nil {
		return false
	}

	if el.len == 0 {
		el.head = event
		el.len++
		return true
	}

	if el.head.Compare(event) > 0 {
		event.next = el.head
		el.head = event
		el.len++
		return true
	}

	cur := el.head

	for cur != nil {
		if cur.Compare(event) == 0 {
			return false
		}

		if cur.Compare(event) < 0 {
			if cur.next == nil {
				cur.next = event
				el.len++
				return true
			}

			if cur.next.Compare(event) > 0 {
				event.next = cur.next
				cur.next = event
				el.len++
				return true
			}
		}

		cur = cur.next
	}

	cur.next = event
	el.len++

	return true
}

func (el *EventList) Delete(timee string) bool {
	event, err := NewEvent(timee, "")
	if err != nil {
		return false
	}

	if el.head.Compare(event) == 0 {
		if el.len <= 1 {
			el.head = nil
		} else {
			el.head = el.head.next
		}

		el.len--
		return true
	}

	searched := el.searchPrev(event.Time)
	if searched == nil {
		return false
	}

	if searched.next.next != nil {
		searched.next = searched.next.next
	} else {
		searched.next = nil
	}

	return true
}

func (el *EventList) Update(timee, desc string) bool {
	event, err := NewEvent(timee, desc)
	if err != nil {
		return false
	}

	searched := el.Search(event.Time)
	if searched == nil {
		return false
	}
	searched.Description = event.Description
	return true
}

func (el *EventList) Search(timee time.Time) *Event {
	var err error

	if y, m, d := timee.Date(); y != 1 || m != 1 || d != 1 {
		timee, err = time.Parse(time.TimeOnly, timee.Format(time.TimeOnly))
		if err != nil {
			return nil
		}
	}

	cur := el.head
	for cur != nil {
		if cur.Time.Compare(timee) == 0 {
			return cur
		}
		cur = cur.next
	}

	return nil
}

func (el *EventList) searchPrev(time time.Time) *Event {
	cur := el.head
	for cur.next != nil {
		if cur.next.Time.Compare(time) == 0 {
			return cur
		}
		cur = cur.next
	}

	return nil
}

func (el *EventList) MarshalJSON() ([]byte, error) {
	var events []*Event

	cur := el.head
	for cur != nil {
		events = append(events, cur)
		cur = cur.next
	}

	return json.Marshal(events)
}

func (el *EventList) String() string {
	builder := strings.Builder{}
	cur := el.head
	for cur != nil {
		builder.WriteString(cur.Description + " ")
		cur = cur.next
	}

	return strings.TrimSpace(builder.String())
}
