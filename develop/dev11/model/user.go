package model

import (
	"errors"
	"strconv"
	"time"
)

type User struct {
	Id     int                   `json:"user_id"`
	Events map[string]*EventList `json:"events"`
}

func NewUser(id int) *User {
	return &User{
		Id:     id,
		Events: make(map[string]*EventList),
	}
}

func (user *User) EventsByDate(date string) (map[string]*EventList, error) {
	dateAsTime, err := time.Parse(time.DateOnly, date)
	if err != nil {
		return nil, err
	}

	events := user.getEventInterval(dateAsTime, dateAsTime)

	return events, nil
}

func (user *User) EventsByWeek(date string) (map[string]*EventList, error) {
	dateAsTime, err := time.Parse(time.DateOnly, date)
	if err != nil {
		return nil, err
	}

	events := user.getEventInterval(dateAsTime, dateAsTime.AddDate(0, 0, 7))

	return events, nil
}

func (user *User) EventsByMonth(date string) (map[string]*EventList, error) {
	dateAsTime, err := time.Parse(time.DateOnly, date)
	if err != nil {
		return nil, err
	}

	events := user.getEventInterval(dateAsTime, dateAsTime.AddDate(0, 1, 0))

	return events, nil
}

func (user *User) getEventInterval(start, end time.Time) map[string]*EventList {
	events := make(map[string]*EventList)
	for start.Compare(end) <= 0 {
		date := start.Format(time.DateOnly)
		if eventList, ok := user.Events[date]; ok {
			events[date] = eventList
		}
		start = start.Add(time.Hour * 24)
	}

	return events
}

func (user *User) AddEvent(timee, description string) error {
	dateTimeAsTime, err := time.Parse(time.DateTime, timee)
	if err != nil {
		return err
	}
	date := dateTimeAsTime.Format(time.DateOnly)

	if _, ok := user.Events[date]; !ok {
		user.Events[date] = NewEventList()
	}

	if !user.Events[date].Add(dateTimeAsTime.Format(time.TimeOnly), description) {
		return errors.New("cannot add event")
	}

	return nil
}

func (user *User) UpdateEvent(timee, description string) error {
	dateTimeAsTime, err := time.Parse(time.DateTime, timee)
	if err != nil {
		return err
	}
	date := dateTimeAsTime.Format(time.DateOnly)

	if _, ok := user.Events[date]; !ok {
		return errors.New("date not found")
	}

	if !user.Events[date].Update(dateTimeAsTime.Format(time.TimeOnly), description) {
		return errors.New("cannot update event")
	}

	return nil
}

func (user *User) DeleteEvent(timee string) error {
	dateTimeAsTime, err := time.Parse(time.DateTime, timee)
	if err != nil {
		return err
	}
	date := dateTimeAsTime.Format(time.DateOnly)

	if _, ok := user.Events[date]; !ok {
		return errors.New("date not found")
	}

	if !user.Events[date].Delete(dateTimeAsTime.Format(time.TimeOnly)) {
		return errors.New("event not found")
	}

	if user.Events[date].len == 0 {
		delete(user.Events, date)
	}

	return nil
}

type Users map[int]*User

func (users Users) Add(id string) bool {
	userID, err := strconv.Atoi(id)
	if err != nil {
		return false
	}

	if _, ok := users[userID]; ok {
		return false
	}

	users[userID] = NewUser(userID)
	return true
}

func (users Users) Get(id string) *User {
	userID, err := strconv.Atoi(id)
	if err != nil {
		return nil
	}

	if _, ok := users[userID]; !ok {
		return nil
	}

	return users[userID]
}
