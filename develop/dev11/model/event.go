package model

import (
	"encoding/json"
	"time"
)

type Event struct {
	Time        time.Time `json:"time"`
	Description string    `json:"description"`
	next        *Event
}

func NewEvent(timee, description string) (*Event, error) {
	timeParsed, err := time.Parse(time.TimeOnly, timee)
	if err != nil {
		return nil, err
	}
	return &Event{
		Time:        timeParsed,
		Description: description,
		next:        nil,
	}, nil
}

func (event *Event) String() string {
	return event.Time.Format(time.TimeOnly) + " " + event.Description
}

func (event *Event) MarshalJSON() ([]byte, error) {
	jsonStruct := struct {
		Time        string `json:"time"`
		Description string `json:"description"`
	}{
		event.Time.Format(time.TimeOnly),
		event.Description,
	}

	return json.Marshal(jsonStruct)
}

func (event *Event) Compare(eventCompare *Event) int {
	return event.Time.Compare(eventCompare.Time)
}
