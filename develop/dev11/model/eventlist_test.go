package model

import (
	"testing"
	"time"
)

var timeee, _ = time.Parse(time.TimeOnly, "00:00:01.000000")

//TODO: Убрать костыли и придумать метод по созданию Event с next

func TestEventListAdd(t *testing.T) {
	tests := []struct {
		name     string
		list     *EventList
		expected string
	}{
		{
			"AddToEmpty",
			&EventList{len: 0, head: nil},
			"b",
		},
		{
			"AddToEnd",
			&EventList{1, eventWithoutError(time.Now().Format(time.TimeOnly), "a")},
			"a b",
		},
		{
			"AddToStart",
			&EventList{1, eventWithoutError(time.Now().Add(time.Minute*5).Format(time.TimeOnly), "a")},
			"b a",
		},
		{
			"UnsortedAdd",
			&EventList{3,
				&Event{
					timeFromTime(time.Now()),
					"a",
					&Event{
						timeFromTime(time.Now().Add(time.Minute)),
						"c",
						&Event{
							timeFromTime(time.Now().Add(time.Minute)),
							"d",
							nil}}},
			},
			"a b c d",
		},
		{
			"ReplaceAdd",
			&EventList{1, eventWithoutError(timeee.Format(time.TimeOnly), "a")},
			"a",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			if test.name == "ReplaceAdd" {
				test.list.Add(timeee.Format(time.TimeOnly), "b")
			} else {
				test.list.Add(time.Now().Add(time.Second*2).Format(time.TimeOnly), "b")
			}
			if test.expected != test.list.String() {
				t.Errorf("Not equal exp and event: %v - %v", test.expected, test.list.String())
			}
		})
	}
}

func TestEventListDelete(t *testing.T) {
	tests := []struct {
		name     string
		list     *EventList
		delete   string
		expected string
	}{
		{
			"DeleteOnceEvent",
			&EventList{1, eventWithoutError(timeee.Format(time.TimeOnly), "a")},
			timeee.Format(time.TimeOnly),
			"",
		},
		{
			"DeleteFirst",
			&EventList{3,
				&Event{
					timeFromTime(timeee),
					"a",
					&Event{
						timeFromTime(time.Now().Add(time.Minute)),
						"c",
						&Event{
							timeFromTime(time.Now().Add(time.Minute)),
							"d",
							nil}}},
			},
			timeee.Format(time.TimeOnly),
			"c d",
		},
		{
			"DeleteLast",
			&EventList{3,
				&Event{
					timeFromTime(time.Now()),
					"a",
					&Event{
						timeFromTime(time.Now().Add(time.Minute)),
						"c",
						&Event{
							timeFromTime(timeee),
							"d",
							nil}}},
			},
			timeee.Format(time.TimeOnly),
			"a c",
		},
		{
			"DeleteMid",
			&EventList{3,
				&Event{
					timeFromTime(time.Now()),
					"a",
					&Event{
						timeFromTime(timeee),
						"c",
						&Event{
							timeFromTime(time.Now().Add(time.Minute)),
							"d",
							nil}}},
			},
			timeee.Format(time.TimeOnly),
			"a d",
		},
		{
			"DeleteNotExist",
			&EventList{3,
				&Event{
					timeFromTime(time.Now()),
					"a",
					&Event{
						timeFromTime(time.Now().Add(time.Minute)),
						"c",
						&Event{
							timeFromTime(time.Now().Add(time.Minute)),
							"d",
							nil}}},
			},
			timeee.Format(time.TimeOnly),
			"a c d",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			test.list.Delete(test.delete)
			if test.expected != test.list.String() {
				t.Errorf("Not equal exp and event: %v - %v", test.expected, test.list.String())
			}
		})
	}
}

func TestEventListUpdate(t *testing.T) {
	tests := []struct {
		name     string
		list     *EventList
		update   *Event
		expected string
	}{
		{
			"UpdateFirst",
			&EventList{3,
				&Event{
					timeFromTime(timeee),
					"a",
					&Event{
						timeFromTime(time.Now().Add(time.Minute)),
						"c",
						&Event{
							timeFromTime(time.Now().Add(time.Minute)),
							"d",
							nil}}},
			},
			eventWithoutError(timeee.Format(time.TimeOnly), "f"),
			"f c d",
		},
		{
			"UpdateLast",
			&EventList{3,
				&Event{
					timeFromTime(time.Now()),
					"a",
					&Event{
						timeFromTime(time.Now().Add(time.Minute)),
						"c",
						&Event{
							timeFromTime(timeee),
							"d",
							nil}}},
			},
			eventWithoutError(timeee.Format(time.TimeOnly), "f"),
			"a c f",
		},
		{
			"UpdateMid",
			&EventList{3,
				&Event{
					timeFromTime(time.Now()),
					"a",
					&Event{
						timeFromTime(timeee),
						"c",
						&Event{
							timeFromTime(time.Now().Add(time.Minute)),
							"d",
							nil}}},
			},
			eventWithoutError(timeee.Format(time.TimeOnly), "f"),
			"a f d",
		},
		{
			"UpdateNotExisted",
			&EventList{3, &Event{
				time.Now(),
				"a",
				&Event{
					time.Now().Add(time.Minute),
					"c",
					&Event{
						time.Now().Add(time.Minute),
						"d",
						nil}}},
			},
			eventWithoutError(timeee.Format(time.TimeOnly), "f"),
			"a c d",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			test.list.Update(test.update.Time.Format(time.TimeOnly), test.update.Description)
			if test.expected != test.list.String() {
				t.Errorf("Not equal exp and event: %v - %v", test.expected, test.list.String())
			}
		})
	}
}

func eventWithoutError(timee, desc string) *Event {
	ev, _ := NewEvent(timee, desc)
	return ev
}

func timeFromTime(time2 time.Time) time.Time {
	res, _ := time.Parse(time.TimeOnly, time2.Format(time.TimeOnly))
	return res
}
