package main

import (
	"encoding/json"
	"fmt"
	"log/slog"
	"net/http"
	"time"
)

type Error struct {
	Err struct {
		Timestamp   time.Time `json:"timestamp"`
		Description string    `json:"description"`
		Url         string    `json:"url"`
		Method      string    `json:"method"`
		Body        string    `json:"body"`
	} `json:"error"`
}

func NewError(desc string, r *http.Request) *Error {
	err := struct {
		Timestamp   time.Time `json:"timestamp"`
		Description string    `json:"description"`
		Url         string    `json:"url"`
		Method      string    `json:"method"`
		Body        string    `json:"body"`
	}{
		Timestamp:   time.Now(),
		Description: desc,
		Url:         r.URL.String(),
		Method:      r.Method,
		Body:        BodyToString(r),
	}

	return &Error{
		err,
	}
}

func (error *Error) Error() string {
	return fmt.Sprintf("%v\tError: %v, URL: %v, Method: %v, Body: %v", error.Err.Timestamp, error.Err.Description, error.Err.Url, error.Err.Method, error.Err.Body)
}

func (error *Error) Marshal() string {
	res, err := json.MarshalIndent(error, "", "  ")
	if err != nil {
		slog.Debug("Marshal error...", "error", err)
	}

	return string(res)
}

func InternalServerError(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusInternalServerError)
	if _, err := w.Write([]byte("InternalServerError")); err != nil {
		slog.Debug(err.Error())
	}
}

func BadRequest(w http.ResponseWriter, r *http.Request, error *Error) {
	w.WriteHeader(http.StatusBadRequest)
	if _, err := w.Write([]byte(error.Marshal())); err != nil {
		slog.Debug(err.Error())
	}
}

func BusinessError(w http.ResponseWriter, r *http.Request, error *Error) {
	w.WriteHeader(http.StatusServiceUnavailable)
	if _, err := w.Write([]byte(error.Marshal())); err != nil {
		slog.Debug(err.Error())
	}
}
