package main

import (
	"log/slog"
	"net/http"
)

type Middleware func(handler http.Handler) http.Handler

type Chain []Middleware

func CreateChain(middleware ...Middleware) Chain {
	var chain []Middleware
	return append(chain, middleware...)
}

func (chain Chain) Then(originalHandler http.Handler) http.Handler {
	if originalHandler == nil {
		originalHandler = http.DefaultServeMux
	}

	for i := range chain {
		originalHandler = chain[len(chain)-1-i](originalHandler)
	}

	return originalHandler
}

func LoggingHandle(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		slog.Info("Start handle request:", "url", r.URL, "method", r.Method, "body", BodyToString(r))
		handler.ServeHTTP(w, r)
		slog.Info("End handle request:", "url", r.URL, "method", r.Method, "body", BodyToString(r))
	})
}

func ValidateMethodGet(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		slog.Info("Validate method GET:", "url", r.URL, "method", r.Method, "body", BodyToString(r))
		if r.Method != http.MethodGet {
			err := NewError("Method not allowed. Allowed GET.", r)
			slog.Error(err.Error())
			BadRequest(w, r, err)
			return
		}

		handler.ServeHTTP(w, r)
	})
}

func ValidateMethodPost(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		slog.Info("Validate method POST:", "url", r.URL.String(), "method", r.Method, "body", BodyToString(r))
		if r.Method != http.MethodPost {
			err := NewError("Method not allowed. Allowed POST.", r)
			slog.Error(err.Error())
			BadRequest(w, r, err)
			return
		}

		handler.ServeHTTP(w, r)
	})
}

func PanicHandler(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if err := recover(); err != nil {
				slog.Error("Panic detected:", "error", err)
				InternalServerError(w, r)
				return
			}

			handler.ServeHTTP(w, r)
		}()
	})
}

func SetCookieResponse(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		slog.Debug("Set headers to response...")
		w.Header().Set("Content-Type", "application/json")
		slog.Debug("Set header:", "Content-Type", w.Header().Get("Content-Type"))
		handler.ServeHTTP(w, r)
	})
}

func BodyValidate(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		slog.Info("Validate body")
		bd, err := NewBodyFromRequest(r)
		if err != nil {
			errr := NewError(err.Error(), r)
			slog.Error("Not valid body:", "error", err.Error())
			BadRequest(w, r, errr)
			return
		}

		if err = bd.Validate(); err != nil {
			errr := NewError(err.Error(), r)
			slog.Error("Not valid body:", "error", err.Error())
			BadRequest(w, r, errr)
			return
		}
		slog.Info("Body is valid")
		handler.ServeHTTP(w, r)
	})
}

func UrlQueryValidate(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		slog.Info("Validate url query")
		bd, err := NewUrlDataFromRequest(r)
		if err != nil {
			errr := NewError(err.Error(), r)
			slog.Error("Not valid url query:", "error", err.Error())
			BadRequest(w, r, errr)
			return
		}

		if err = bd.Validate(); err != nil {
			errr := NewError(err.Error(), r)
			slog.Error("Not valid url query data:", "error", err.Error())
			BadRequest(w, r, errr)
			return
		}
		slog.Info("UrlData is valid")
		handler.ServeHTTP(w, r)
	})
}
