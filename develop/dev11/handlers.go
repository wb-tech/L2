package main

import (
	"dev11/model"
	"encoding/json"
	"errors"
	"log/slog"
	"net/http"
)

var users = make(model.Users)

// TODO: Добавить тип result
func CreateEvent(w http.ResponseWriter, r *http.Request) {
	body, _ := NewBodyFromRequest(r)

	users.Add(body.UserId)
	user := users.Get(body.UserId)

	if err := user.AddEvent(body.Date, body.EventDesc); err != nil {
		errr := NewError(err.Error(), r)
		slog.Error("Cannot add event for user:", "user_id", body.UserId, "error", errr)
		BusinessError(w, r, errr)
		return
	}

	res, err := json.Marshal(NewOperationResult(body.UserId, "Event added to user"))
	if err != nil {
		errr := NewError(err.Error(), r)
		slog.Error("Cannot format result to response:", "user_id", body.UserId, "error", errr)
		BusinessError(w, r, errr)
		return
	}

	w.WriteHeader(http.StatusOK)
	if _, err = w.Write(res); err != nil {
		errr := NewError(err.Error(), r)
		slog.Error("Cannot write result to response:", "user_id", body.UserId, "error", errr)
		BusinessError(w, r, errr)
		return
	}
}

func UpdateEvent(w http.ResponseWriter, r *http.Request) {
	body, _ := NewBodyFromRequest(r)

	user := users.Get(body.UserId)

	if user == nil {
		errr := NewError(errors.New("not found user").Error(), r)
		slog.Error("Cannot update event for user:", "user_id", body.UserId, "error", errr)
		BadRequest(w, r, errr)
		return
	}

	if err := user.UpdateEvent(body.Date, body.EventDesc); err != nil {
		errr := NewError(err.Error(), r)
		slog.Error("Cannot update event for user:", "user_id", body.UserId, "error", errr)
		BusinessError(w, r, errr)
		return
	}

	res, err := json.Marshal(NewOperationResult(body.UserId, "Event updated..."))
	if err != nil {
		errr := NewError(err.Error(), r)
		slog.Error("Cannot format result to response:", "user_id", body.UserId, "error", errr)
		BusinessError(w, r, errr)
		return
	}

	w.WriteHeader(http.StatusOK)
	if _, err = w.Write(res); err != nil {
		errr := NewError(err.Error(), r)
		slog.Error("Cannot write result to response:", "user_id", body.UserId, "error", errr)
		BusinessError(w, r, errr)
		return
	}
}

func DeleteEvent(w http.ResponseWriter, r *http.Request) {
	body, _ := NewBodyFromRequest(r)

	user := users.Get(body.UserId)

	if user == nil {
		errr := NewError(errors.New("not found user").Error(), r)
		slog.Error("Cannot update event for user:", "user_id", body.UserId, "error", errr)
		BadRequest(w, r, errr)
		return
	}

	if err := user.DeleteEvent(body.Date); err != nil {
		errr := NewError(err.Error(), r)
		slog.Error("Cannot update event for user:", "user_id", body.UserId, "error", errr)
		BusinessError(w, r, errr)
		return
	}

	res, err := json.Marshal(NewOperationResult(body.UserId, "Event deleted from user"))
	if err != nil {
		errr := NewError(err.Error(), r)
		slog.Error("Cannot format result to response:", "user_id", body.UserId, "error", errr)
		BusinessError(w, r, errr)
		return
	}

	w.WriteHeader(http.StatusOK)
	if _, err = w.Write(res); err != nil {
		errr := NewError(err.Error(), r)
		slog.Error("Cannot write result to response:", "user_id", body.UserId, "error", errr)
		BusinessError(w, r, errr)
		return
	}
}

func GetDayEvents(w http.ResponseWriter, r *http.Request) {
	urlData, _ := NewUrlDataFromRequest(r)

	user := users.Get(urlData.UserId)

	if user == nil {
		errr := NewError(errors.New("not found user").Error(), r)
		slog.Error("Cannot get event for user:", "user_id", urlData.UserId, "error", errr)
		BadRequest(w, r, errr)
		return
	}

	list, err := user.EventsByDate(urlData.Date)
	if err != nil {
		errr := NewError(err.Error(), r)
		slog.Error("Cannot get event for user:", "user_id", urlData.UserId, "error", errr)
		BusinessError(w, r, errr)
		return
	}

	res, err := json.Marshal(NewReadResult(urlData.UserId, list))
	if err != nil {
		errr := NewError(err.Error(), r)
		slog.Error("Cannot format result to response:", "user_id", urlData.UserId, "error", errr)
		BusinessError(w, r, errr)
		return
	}

	w.WriteHeader(http.StatusOK)
	if _, err = w.Write(res); err != nil {
		errr := NewError(err.Error(), r)
		slog.Error("Cannot write result to response:", "user_id", urlData.UserId, "error", errr)
		BusinessError(w, r, errr)
		return
	}
}

func GetWeekEvents(w http.ResponseWriter, r *http.Request) {
	urlData, _ := NewUrlDataFromRequest(r)

	user := users.Get(urlData.UserId)

	if user == nil {
		errr := NewError(errors.New("not found user").Error(), r)
		slog.Error("Cannot update event for user:", "user_id", urlData.UserId, "error", errr)
		BadRequest(w, r, errr)
		return
	}

	list, err := user.EventsByWeek(urlData.Date)
	if err != nil {
		errr := NewError(err.Error(), r)
		slog.Error("Cannot update event for user:", "user_id", urlData.UserId, "error", errr)
		BusinessError(w, r, errr)
		return
	}

	res, err := json.Marshal(NewReadResult(urlData.UserId, list))
	if err != nil {
		errr := NewError(err.Error(), r)
		slog.Error("Cannot format result to response:", "user_id", urlData.UserId, "error", errr)
		BusinessError(w, r, errr)
		return
	}

	w.WriteHeader(http.StatusOK)
	if _, err = w.Write(res); err != nil {
		errr := NewError(err.Error(), r)
		slog.Error("Cannot write result to response:", "user_id", urlData.UserId, "error", errr)
		BusinessError(w, r, errr)
		return
	}
}

func GetMonthEvents(w http.ResponseWriter, r *http.Request) {
	urlData, _ := NewUrlDataFromRequest(r)

	user := users.Get(urlData.UserId)

	if user == nil {
		errr := NewError(errors.New("not found user").Error(), r)
		slog.Error("Cannot update event for user:", "user_id", urlData.UserId, "error", errr)
		BadRequest(w, r, errr)
		return
	}

	list, err := user.EventsByMonth(urlData.Date)
	if err != nil {
		errr := NewError(err.Error(), r)
		slog.Error("Cannot update event for user:", "user_id", urlData.UserId, "error", errr)
		BusinessError(w, r, errr)
		return
	}

	res, err := json.Marshal(NewReadResult(urlData.UserId, list))
	if err != nil {
		errr := NewError(err.Error(), r)
		slog.Error("Cannot format result to response:", "user_id", urlData.UserId, "error", errr)
		BusinessError(w, r, errr)
		return
	}

	w.WriteHeader(http.StatusOK)
	if _, err = w.Write(res); err != nil {
		errr := NewError(err.Error(), r)
		slog.Error("Cannot write result to response:", "user_id", urlData.UserId, "error", errr)
		BusinessError(w, r, errr)
		return
	}
}
