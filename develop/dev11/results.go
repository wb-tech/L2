package main

import (
	"dev11/model"
)

type OperationResult struct {
	Result struct {
		UserID          string `json:"user_id"`
		ResultOperation string `json:"result_operation"`
	} `json:"result"`
}

func NewOperationResult(userID, resultOperation string) *OperationResult {
	return &OperationResult{Result: struct {
		UserID          string `json:"user_id"`
		ResultOperation string `json:"result_operation"`
	}{
		UserID:          userID,
		ResultOperation: resultOperation,
	}}
}

type ReadResult struct {
	Result struct {
		UserID string                      `json:"user_id"`
		Events map[string]*model.EventList `json:"events"`
	} `json:"result"`
}

func NewReadResult(userID string, events map[string]*model.EventList) *ReadResult {
	return &ReadResult{Result: struct {
		UserID string                      `json:"user_id"`
		Events map[string]*model.EventList `json:"events"`
	}{
		UserID: userID,
		Events: events,
	}}
}
