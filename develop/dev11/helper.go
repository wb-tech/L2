package main

import (
	"bytes"
	"github.com/go-playground/validator/v10"
	"io"
	"net/http"
	"strings"
)

type Body struct {
	UserId    string `json:"user_id" validate:"required,number"`
	Date      string `json:"date" validate:"required,datetime=2006-01-02|datetime=2006-01-02 15:04:05"`
	EventDesc string `json:"event_desc" validate:"omitempty,alphanumunicode"`
}

func NewBodyFromRequest(r *http.Request) (*Body, error) {
	if err := r.ParseForm(); err != nil {
		return nil, err
	}

	r.Body = io.NopCloser(strings.NewReader(r.PostForm.Encode()))

	bd := &Body{r.PostForm.Get("user_id"), r.PostForm.Get("date"), r.PostForm.Get("desc")}

	return bd, nil
}

func (bd *Body) Validate() error {
	valid := validator.New()
	if err := valid.Struct(bd); err != nil {
		return err
	}
	return nil
}

type UrlData struct {
	UserId string `json:"user_id" validate:"required,number"`
	Date   string `json:"date" validate:"required,datetime=2006-01-02"`
}

func NewUrlDataFromRequest(r *http.Request) (*UrlData, error) {
	if err := r.ParseForm(); err != nil {
		return nil, err
	}

	urlData := &UrlData{UserId: r.Form.Get("user_id"), Date: r.Form.Get("date")}

	return urlData, nil
}

func (url *UrlData) Validate() error {
	valid := validator.New()
	if err := valid.Struct(url); err != nil {
		return err
	}
	return nil
}

func BodyToString(r *http.Request) string {
	bd, err := io.ReadAll(r.Body)
	if err != nil {
		return ""
	}
	body := io.NopCloser(bytes.NewReader(bd))
	r.Body = body

	return string(bd)
}
