package main

import "testing"

func TestStartNumUnpack(t *testing.T) {
	if res, err := unpack("45dfsggd"); err == nil && res != "" {
		t.Errorf("not error with start from unpack number: %v", res)
	}
}

func TestEmptyString(t *testing.T) {
	if res, err := unpack(""); err != nil && res != "" {
		t.Errorf("not error with start from unpack number: %v", res)
	}
}

func TestZeroUnpackNum(t *testing.T) {
	if res, err := unpack("d0213fsggd"); err != nil && res != "" {
		t.Errorf("not error with zero unpack number: %v", res)
	}
}

func TestWithoutEscape(t *testing.T) {
	tests := []struct {
		name   string
		pack   string
		unpack string
	}{
		{
			"FirstCheck",
			"a4bc2d5e",
			"aaaabccddddde",
		},
		{
			"SecondCheck",
			"abcd",
			"abcd",
		},
		{
			"ThirdCheck",
			"abcd55c10",
			"abcdddddddddddddddddddddddddddddddddddddddddddddddddddddddcccccccccc",
		},
		{
			"FourthCheck",
			"a1",
			"a",
		},
		{
			"FifthCheck",
			"a1b2c3d4",
			"abbcccdddd",
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			res, err := unpack(test.pack)
			if err != nil {
				t.Errorf("error: %v, resulted:%v, excepted: %v", err, res, test.unpack)
			}

			if res != test.unpack {
				t.Errorf("not equal resulted and expected: %v \t %v", res, test.unpack)
			}
		})
	}
}

func TestWithEscape(t *testing.T) {
	tests := []struct {
		name   string
		pack   string
		unpack string
	}{
		{
			"FirstCheckWithEsc",
			"qwe\\4\\5",
			"qwe45",
		},
		{
			"SecondCheckWithEsc",
			`qwe\45`,
			"qwe44444",
		},
		{
			"ThirdCheckWithEsc",
			`ab\55c4cdtf\6fgd\\3`,
			`ab55555cccccdtf6fgd\\\`,
		},
		{
			"FourthCheckWithEsc",
			`qwe\\5`,
			`qwe\\\\\`,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			res, err := unpack(test.pack)
			if err != nil {
				t.Errorf("error: %v, resulted:%v, excepted: %v", err, res, test.unpack)
			}

			if res != test.unpack {
				t.Errorf("not equal resulted and expected: %v \t %v", res, test.unpack)
			}
		})
	}
}
