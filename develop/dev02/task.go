package main

import (
	"errors"
	"fmt"
	"log"
	"unicode"
)

/*
=== Задача на распаковку ===

Создать Go функцию, осуществляющую примитивную распаковку строки, содержащую повторяющиеся символы / руны, например:
	- "a4bc2d5e" => "aaaabccddddde"
	- "abcd" => "abcd"
	- "45" => "" (некорректная строка)
	- "" => ""
Дополнительное задание: поддержка escape - последовательностей
	- qwe\4\5 => qwe45 (*)
	- qwe\45 => qwe44444 (*)
	- qwe\\5 => qwe\\\\\ (*)

В случае если была передана некорректная строка функция должна возвращать ошибку. Написать unit-тесты.

Функция должна проходить все тесты. Код должен проходить проверки go vet и golint.
*/

func main() {
	var str string
	print("Enter string for unpack: ")
	if _, err := fmt.Scan(&str); err != nil {
		log.Panic(err)
	}

	res, err := unpack(str)
	if err != nil {
		log.Panic(err)
	}

	println(res)
}

func unpack(str string) (string, error) {
	var res []rune
	var ch rune
	rns := []rune(str)

	if len(str) == 0 {
		return "", nil
	}

	if unicode.IsDigit(rns[0]) {
		return "", errors.New("string for unpack cannot start with unpack number")
	}

	for i := 0; i < len(rns); i++ {
		if rns[i] == '\\' {
			ch = rns[i+1]
			i++
		} else if unicode.IsDigit(rns[i]) {

			if rns[i]-'0' == 0 {
				return "", errors.New("unpack number cannot start with zero")
			}

			ln, num := getUnpackNum(rns[i:])

			if num == 1 {
				continue
			}

			for j := 0; j < int(num)-2; j++ {
				res = append(res, ch)
			}
			i += ln - 1
		} else {
			ch = rns[i]
		}

		res = append(res, ch)

	}

	return string(res), nil
}

func getUnpackNum(rns []rune) (int, int) {
	i := 0
	res := 0
	for len(rns) > i && unicode.IsDigit(rns[i]) {
		res = res*10 + int(rns[i]-'0')
		i++
	}

	return i, res
}
