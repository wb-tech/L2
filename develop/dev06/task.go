package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
)

/*
=== Утилита cut ===

Принимает STDIN, разбивает по разделителю (TAB) на колонки, выводит запрошенные

Поддержать флаги:
-f - "fields" - выбрать поля (колонки)
-d - "delimiter" - использовать другой разделитель
-s - "separated" - только строки с разделителем

Программа должна проходить все тесты. Код должен проходить проверки go vet и golint.
*/

var (
	sep    = flag.Bool("s", false, "Only string with separator")
	delim  = flag.String("d", "\t", "Delimiter for fields")
	fields = flag.String("f", "", "Fields for print")
)

var (
	intervalRegex = regexp.MustCompile(`(\d+-\d+)|(\d+-)|(-\d+)|(\d+)`)
)

func main() {
	flag.Parse()
	flag.Usage = func() {
		fmt.Println("Program for print selected line columns separated by delimiter")
		flag.PrintDefaults()
	}

	var cut = &Cut{sep: *sep, fields: *fields, del: *delim}

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		cut.Add(scanner.Text())
	}
	if err := scanner.Err(); err != nil {
		log.Fatalln(err)
	}

	fmt.Println(cut.Cut())

}

type Cut struct {
	sep    bool
	fields string
	del    string
	lines  []string
}

func (ct *Cut) Add(line string) {
	ct.lines = append(ct.lines, line)
}

func (ct *Cut) Cut() ([]string, error) {
	var err error
	var cut Cuter
	var fil Filter
	var mrg Merger

	//TODO: Может быть вынести алгоритм в шаблонный метод
	if ct.sep {
		cut = OnlySep{}
	} else {
		cut = Sep{}
	}

	cuts := cut.Cut(ct.lines, ct.del)
	if ct.fields != "" {
		cuts, err = fil.Filter(cuts, ct.fields)
	}
	if err != nil {
		return nil, err
	}

	mrg = Join{}

	return mrg.Merge(cuts, ct.del), nil

}
