package main

import "strings"

type Cuter interface {
	Cut(lines []string, del string) [][]string
}

type Sep struct{}

func (ons Sep) Cut(lines []string, del string) [][]string {
	var res [][]string

	for _, line := range lines {
		res = append(res, strings.Split(line, del))
	}

	return res
}

type OnlySep struct{}

func (ons OnlySep) Cut(lines []string, del string) [][]string {
	var res [][]string

	for _, line := range lines {
		if !strings.Contains(line, del) {
			continue
		}

		res = append(res, strings.Split(line, del))
	}

	return res
}
