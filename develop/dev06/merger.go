package main

import "strings"

type Merger interface {
	Merge(cuts [][]string, del string) []string
}

type Join struct{}

func (Join) Merge(cuts [][]string, del string) []string {
	var res []string

	for _, cut := range cuts {
		res = append(res, strings.Join(cut, del))
	}

	return res
}
