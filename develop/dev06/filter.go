package main

import (
	"errors"
	"regexp"
	"strconv"
	"strings"
)

var filterReg = regexp.MustCompile(`^( +([0-9]+-[0-9]+|[0-9]+-|-[0-9]+|[0-9]+))+$`)
var splitReg = regexp.MustCompile(`([0-9]+-[0-9]+|[0-9]+-|-[0-9]+|[0-9]+)`)

var (
	oneReg      = regexp.MustCompile(`[0-9]+`)
	oneToInfReg = regexp.MustCompile(`[0-9]+-`)
	infToOneReg = regexp.MustCompile(`-[0-9]+`)
	oneToOneReg = regexp.MustCompile(`[0-9]+-[0-9]+`)
)

type Filter struct{}

func (fl Filter) Filter(cuts [][]string, filter string) ([][]string, error) {
	if !filterReg.MatchString(" " + filter) {
		return nil, errors.New("not valid fields selector")
	}

	mask, err := fl.maskFields(splitReg.FindAllString(filter, -1), fl.getMax(cuts))
	if err != nil {
		return nil, err
	}

	return fl.maskedFields(mask, cuts), nil
}

func (fl Filter) maskFields(fds []string, m int) ([]bool, error) {
	mask := make([]bool, m)
	for _, fd := range fds {
		switch {
		case oneToOneReg.MatchString(fd):
			inter := strings.Split(fd, "-")
			ind1, err := strconv.Atoi(inter[0])
			if err != nil {
				return nil, err
			}

			ind2, err := strconv.Atoi(inter[1])
			if err != nil {
				return nil, err
			}

			if ind1 > ind2 {
				return nil, errors.New("interval must be increase")
			}

			if len(mask) < ind1 {
				continue
			}

			if len(mask) < ind2 {
				ind2 = len(mask)
			}

			slice := mask[ind1-1 : ind2]
			for i, _ := range slice {
				slice[i] = true
			}
		case oneToInfReg.MatchString(fd):
			inter := strings.Replace(fd, "-", "", -1)
			ind, err := strconv.Atoi(inter)
			if err != nil {
				return nil, err
			}

			if len(mask) < ind {
				continue
			}

			slice := mask[ind-1:]
			for i, _ := range slice {
				slice[i] = true
			}

		case infToOneReg.MatchString(fd):
			inter := strings.Replace(fd, "-", "", -1)
			ind, err := strconv.Atoi(inter)
			if err != nil {
				return nil, err
			}

			if len(mask) < ind {
				ind = len(mask)
			}

			slice := mask[:ind]
			for i, _ := range slice {
				slice[i] = true
			}
		case oneReg.MatchString(fd):
			ind, err := strconv.Atoi(fd)
			if err != nil {
				return nil, err
			}

			if len(mask) < ind {
				continue
			}

			mask[ind-1] = true
		default:
			return nil, errors.New("not valid interval")
		}

	}

	return mask, nil
}

func (Filter) getMax(cuts [][]string) int {
	var m int
	for _, cut := range cuts {
		if m < len(cut) {
			m = len(cut)
		}
	}

	return m
}

func (Filter) maskedFields(mask []bool, cuts [][]string) [][]string {
	var res [][]string

	for _, cut := range cuts {
		var filtered []string
		for i := range cut {
			if mask[i] {
				filtered = append(filtered, cut[i])
			}
		}
		res = append(res, filtered)
	}

	return res
}
