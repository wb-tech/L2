package main

import (
	"bufio"
	"log"
	"os"
	"strings"
)

/*
=== Взаимодействие с ОС ===

Необходимо реализовать собственный шелл

встроенные команды: cd/pwd/echo/kill/ps
поддержать fork/exec команды
конвеер на пайпах

Реализовать утилиту netcat (nc) клиент
принимать данные из stdin и отправлять в соединение (tcp/udp)
Программа должна проходить все тесты. Код должен проходить проверки go vet и golint.
*/

var Commands = []Command{
	{"cd", cd},
	{"pwd", pwd},
	{"echo", echo},
	{"kill", kill},
	{"ps", ps},
	{"exec", exec},
	{"fork", fork},
}

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		args := strings.Split(strings.TrimSpace(scanner.Text()), " ")
		if err := runCommand(args[0], args[1:]); err != nil {
			log.Println(err)
		}
	}
	if err := scanner.Err(); err != nil {
		log.Fatalln(err)
	}
}

func runCommand(cmd string, args []string) error {
	for _, command := range Commands {
		if command.cmd == cmd {
			if err := command.Exec(args); err != nil {
				return err
			}
		}
	}
	return nil
}
