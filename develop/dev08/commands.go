package main

import (
	"errors"
	"fmt"
	"os"
	exec2 "os/exec"
	"path"
	"strconv"
	"strings"
	"syscall"
)

type Command struct {
	cmd    string
	action func(args []string) error
}

func (cmd Command) Exec(args []string) error {
	return cmd.action(args)
}

func cd(args []string) (err error) {
	var path string
	if len(args) == 0 {
		path, err = os.UserHomeDir()
		if err != nil {
			return err
		}
	} else {
		path = args[0]
	}

	if strings.HasPrefix(path, "~") {
		path, err = os.UserHomeDir()
		if err != nil {
			return err
		}
		path = path + args[0][len("~"):]
	}

	return os.Chdir(path)
}

func pwd(args []string) error {
	dir, err := os.Getwd()
	if err != nil {
		return err
	}

	fmt.Println(dir)

	return nil
}

func echo(args []string) error {
	fmt.Println(strings.Join(args, " "))
	return nil
}

func kill(args []string) error {
	for _, pid := range args {
		p, err := strconv.Atoi(pid)
		if err != nil {
			return err
		}

		proc, err := os.FindProcess(p)
		if err != nil {
			return err
		}

		if err = proc.Kill(); err != nil {
			return err
		}
	}
	return nil
}

func ps(args []string) error {
	cmd := exec2.Command("ps", args...)
	if err := cmd.Err; err != nil {
		return err
	}

	bytes, err := cmd.Output()
	if err != nil {
		return err
	}

	fmt.Println(string(bytes))
	return nil
}

func exec(args []string) error {
	if len(args) <= 0 {
		return errors.New("not found command")
	}

	cmd := args[0]

	if !path.IsAbs(cmd) {
		var err error
		paths := strings.Split(os.Getenv("PATH"), ":")
		for _, pt := range paths {
			cmd = pt + "/" + args[0]
			_, err = os.Stat(cmd)
			if err == nil {
				break
			}
		}
		if err != nil {
			return errors.New("not found command")
		}
	}

	if len(args) == 1 {
		return syscall.Exec(cmd, nil, nil)
	} else {
		return syscall.Exec(cmd, args[1:], nil)
	}
}

func fork(args []string) error {
	pid, _, err := syscall.Syscall(syscall.SYS_FORK, 0, 0, 0)
	if err != 0 {
		return err
	}
	fmt.Println(pid)
	return nil
}
