package main

import "testing"

func TestFillMask(t *testing.T) {
	tests := []struct {
		name string
		text *Text
		mask []bool
		exp  []bool
	}{
		{
			"ZeroContext",
			&Text{after: 0, before: 0},
			[]bool{true, false, true},
			[]bool{true, false, true},
		},
		{
			"WithBefore",
			&Text{after: 0, before: 3},
			[]bool{false, false, false, true, false, false, false, true, false, false, false, true, false, false, false},
			[]bool{true, true, true, true, true, true, true, true, true, true, true, true, false, false, false},
		},
		{
			"WithBeforeOver",
			&Text{after: 0, before: 5},
			[]bool{false, false, false, true, false, false, false, true, false, false, false, true, false, false, false},
			[]bool{true, true, true, true, true, true, true, true, true, true, true, true, false, false, false},
		},
		{
			"WithAfter",
			&Text{after: 3, before: 0},
			[]bool{false, false, false, true, false, false, false, true, false, false, false, true, false, false, false},
			[]bool{false, false, false, true, true, true, true, true, true, true, true, true, true, true, true},
		},
		{
			"WithAfterOver",
			&Text{after: 5, before: 0},
			[]bool{false, false, false, true, false, false, false, true, false, false, false, true, false, false, false},
			[]bool{false, false, false, true, true, true, true, true, true, true, true, true, true, true, true},
		},
		{
			"WithContext",
			&Text{after: 3, before: 3},
			[]bool{false, false, false, true, false, false, false, true, false, false, false, true, false, false, false},
			[]bool{true, true, true, true, true, true, true, true, true, true, true, true, true, true, true},
		},
		{
			"WithContextOver",
			&Text{after: 5, before: 5},
			[]bool{false, false, false, true, false, false, false, true, false, false, false, true, false, false, false},
			[]bool{true, true, true, true, true, true, true, true, true, true, true, true, true, true, true},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			res := test.text.fillMask(test.mask)
			for i := range res {
				if res[i] != test.exp[i] {
					t.Errorf("Not equal exp and res in index %v", i)
				}
			}
		},
		)
	}
}
