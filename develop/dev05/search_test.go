package main

import "testing"

func TestRegexpSearch_MaskSearch(t *testing.T) {
	tests := []struct {
		name    string
		text    []string
		ignore  bool
		pattern string
		exp     []bool
	}{
		{
			"SearchCase1",
			[]string{"safsfdaABOBAsdaf", "sadfdsffdsafsd<", "sdfsdaff"},
			false,
			"ABOBA",
			[]bool{true, false, false},
		},
		{
			"SearchCase2",
			[]string{"safsfdaABOBAsdaf", "sadfdsffdsafsd<", "sdfsdABffA"},
			false,
			"AB..A",
			[]bool{true, false, true},
		},
		{
			"SearchCase3",
			[]string{"safsfdaAsdaf", "sadfdsffdsafsd<", "sdfsdABffA"},
			false,
			"A.*A",
			[]bool{false, false, true},
		},
		{
			"SearchCase4",
			[]string{"safsfdasdaf", "sadfdsffdsafsd<", "sdfsdff"},
			false,
			"A.*A",
			[]bool{false, false, false},
		},
		{
			"SearchCase5",
			[]string{`safs\fdasdaf`, `sadfdsffdsafsd<`, "sdfsdff"},
			false,
			`[\\|<]`,
			[]bool{true, true, false},
		},
		{
			"SearchCase6",
			[]string{`safsdasdaf`, `sadfdsffdsafsd`, "sdfsdff"},
			false,
			`[\\|<]`,
			[]bool{false, false, false},
		},
		{
			"SearchCase7",
			[]string{"safsfdaABOBAsdaf", "sadfdsffdsafsd<", "sdfsdaff"},
			true,
			"AbObA",
			[]bool{true, false, false},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			search := NewRegexpSearch(test.pattern, test.ignore)
			res := search.MaskSearch(test.text)
			for i := range res {
				if res[i] != test.exp[i] {
					t.Errorf("Not equal exp and res in index %v", i)
				}
			}
		})
	}
}

//TODO: Маску в отельную сущьность с операциями над маской
//TODO: Тесты
//TODO: Проверка с stdin
//TODO: Проверка линтерами
/*func TestFixSearch_MaskSearch(t *testing.T) {
	tests := []struct {
		text    []string
		pattern string
		res     []bool
	}{}
}
*/
