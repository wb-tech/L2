package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"regexp"
	"strings"
)

/*
=== Утилита grep ===

Реализовать утилиту фильтрации (man grep)

Поддержать флаги:
-A - "after" печатать +N строк после совпадения
-B - "before" печатать +N строк до совпадения
-C - "context" (A+B) печатать ±N строк вокруг совпадения
-c - "count" (количество строк)
-i - "ignore-case" (игнорировать регистр)
-v - "invert" (вместо совпадения, исключать)
-F - "fixed", точное совпадение со строкой, не паттерн
-n - "line num", печатать номер строки

Программа должна проходить все тесты. Код должен проходить проверки go vet и golint.
*/

type Config struct {
	After      int
	Before     int
	Context    int
	Count      bool
	IgnoreCase bool
	Invert     bool
	Fixed      bool
	LineNum    bool
	Pattern    string
	File       string
}

func initConfig() (*Config, error) {
	config := &Config{}
	flag.IntVar(&config.After, "A", 0, "Кол-во строк, которые надо вывести после совпадения")
	flag.IntVar(&config.Before, "B", 0, "Кол-во строк, которые надо вывести до совпадения")
	flag.IntVar(&config.Context, "C", 0, "Кол-во строк, которые надо вывести вокруг совпадения")
	flag.BoolVar(&config.Count, "c", false, "Кол-во строк с совпанением")
	flag.BoolVar(&config.IgnoreCase, "i", false, "Игнорирование регистра")
	flag.BoolVar(&config.Invert, "v", false, "Строки без паттерна")
	flag.BoolVar(&config.Fixed, "F", false, "Точное совпадение(игнорирование паттерна)")
	flag.BoolVar(&config.LineNum, "n", false, "Печатать номер строки")

	flag.Usage = func() {
		fmt.Println("Программа для фильтрации теста с использованием паттернов поиска")
		flag.PrintDefaults()
	}

	flag.Parse()

	if flag.NArg() == 0 {
		return nil, errors.New("не найден паттерн для поиска")
	}

	if _, err := regexp.Compile(flag.Arg(0)); err != nil {
		return nil, errors.New("невалидный паттерн для поиска")
	}

	config.Pattern = flag.Arg(0)

	if flag.NArg() == 1 {
		config.File = "-"
	} else {
		config.File = flag.Arg(1)
	}

	return config, nil
}

func main() {
	var text *Text

	conf, err := initConfig()
	if err != nil {
		log.Panic(err)
	}

	if conf.File == "-" {
		text, err = NewText(os.Stdin, conf)
		if err != nil {
			log.Panic(err)
		}
	} else {
		file, err := os.Open(conf.File)
		if err != nil {
			log.Panic(err)
		}
		defer file.Close()

		text, err = NewText(file, conf)
		if err != nil {
			log.Panic(err)
		}
	}

	if conf.Count {
		fmt.Println(len(text.text))
	} else {
		fmt.Print(text)
	}

}

type Text struct {
	text    []string
	pattern string
	after   int
	before  int
	ignore  bool
	invert  bool
	fixed   bool
	lineNum bool
}

func NewText(reader io.Reader, conf *Config) (*Text, error) {
	text := &Text{}
	scanner := bufio.NewScanner(reader)
	for scanner.Scan() {
		text.text = append(text.text, scanner.Text())
	}
	if err := scanner.Err(); err != nil {
		return nil, err
	}

	text.setConfig(conf)
	text.Action()

	return text, nil
}

func (text *Text) setConfig(conf *Config) {
	// Ставит наибольшее из флагов -C и -A
	if conf.Context > conf.After {
		text.after = conf.Context
	} else {
		text.after = conf.After
	}

	// Ставит наибольшее из флагов -C и -B
	if conf.Context > conf.Before {
		text.before = conf.Context
	} else {
		text.before = conf.Before
	}

	text.fixed = conf.Fixed
	text.ignore = conf.IgnoreCase
	text.invert = conf.Invert
	text.lineNum = conf.LineNum
	text.pattern = conf.Pattern
}

func (text *Text) Action() {
	var search MaskSearcher
	if text.fixed {
		search = NewFixSearch(text.pattern, text.ignore)
	} else {
		search = NewRegexpSearch(text.pattern, text.ignore)
	}
	mask := search.MaskSearch(text.text)
	if text.invert {
		text.invertMask(mask)
	}
	mask = text.fillMask(mask)

	if text.lineNum {
		text.numering()
	}

	text.clear(mask)

}

func (text *Text) numering() {
	for i, k := range text.text {
		text.text[i] = fmt.Sprintf("%v: %v", i, k)
	}
}

// TODO: Бага со смещением относительно последнего true, а не элемента от поискового индекса
func (text *Text) fillMask(mask []bool) []bool {
	prev := mask[0]
	for i := range mask {
		if !prev && mask[i] {
			b := text.before
			if i-b < 0 {
				b = i
			}
			for j := 1; j <= b; j++ {
				mask[i-j] = true
			}
		}

		if prev && !mask[i] {
			a := text.after
			if len(mask)-(i+a) < 0 {
				a = len(mask) - i
			}
			for j := 0; j < a; j++ {
				mask[i+j] = true
			}

		}
		prev = mask[i]
	}
	return mask
}

func (text *Text) invertMask(mask []bool) {
	for i := range mask {
		mask[i] = !mask[i]
	}
}

func (text *Text) clear(mask []bool) {
	var temp []string
	for i, k := range mask {
		if k {
			temp = append(temp, text.text[i])
		}
	}

	text.text = temp
}

func (text *Text) String() string {
	return strings.Join(text.text, "\n")
}
