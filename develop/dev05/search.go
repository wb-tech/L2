package main

import (
	"regexp"
	"strings"
)

type MaskSearcher interface {
	MaskSearch(text []string) []bool
}

type FixSearch struct {
	regex *regexp.Regexp
}

func NewFixSearch(pattern string, ignore bool) *FixSearch {
	replacer := strings.NewReplacer(`.`, `\.`,
		`?`, `\?`,
		`*`, `\*`,
		`+`, `\+`,
		`{`, `\{`,
		`}`, `\}`,
		`|`, `\|`,
		`)`, `\)`,
		`(`, `\(`,
		`[`, `\[`,
		`]`, `\]`,
		`\`, `\\`,
		`^`, `\^`,
		`$`, `\$`)
	patt := replacer.Replace(pattern)
	if ignore {
		patt = "(?i)" + patt
	}
	return &FixSearch{regex: regexp.MustCompile(patt)}
}

func (fix *FixSearch) MaskSearch(text []string) []bool {
	maskIndex := make([]bool, len(text))

	for i, k := range text {
		if fix.regex.MatchString(k) {
			maskIndex[i] = true
		}
	}

	return maskIndex
}

type RegexpSearch struct {
	regex *regexp.Regexp
}

func NewRegexpSearch(pattern string, ignore bool) *RegexpSearch {
	if ignore {
		pattern = "(?i)" + pattern
	}
	return &RegexpSearch{regex: regexp.MustCompile(pattern)}
}

func (fix *RegexpSearch) MaskSearch(text []string) []bool {
	maskIndex := make([]bool, len(text))

	for i, k := range text {
		if fix.regex.MatchString(k) {
			maskIndex[i] = true
		}
	}

	return maskIndex
}
