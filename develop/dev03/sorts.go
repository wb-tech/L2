package main

import (
	"strconv"
	"strings"
)

type NumSort struct {
	file *File
	key  uint
}

func (num *NumSort) Less(i, j int) bool {
	numI, err := strconv.ParseFloat((*num.file)[i][num.key-1], 64)
	if err != nil {
		return false
	}

	numJ, err := strconv.ParseFloat((*num.file)[j][num.key-1], 64)
	if err != nil {
		return true
	}

	if numI >= numJ {
		return false
	}

	return true
}

func (num *NumSort) Len() int {
	return len(*num.file)
}

func (num *NumSort) Swap(i, j int) {
	(*num.file)[i], (*num.file)[j] = (*num.file)[j], (*num.file)[i]
}

//---------------------------------------------------

type StringSort struct {
	file *File
	key  uint
}

func (str *StringSort) Less(i, j int) bool {
	if strings.Compare((*str.file)[i][str.key-1], (*str.file)[j][str.key-1]) != -1 {
		return false
	}

	return true
}

func (str *StringSort) Len() int {
	return len(*str.file)
}

func (str *StringSort) Swap(i, j int) {
	(*str.file)[i], (*str.file)[j] = (*str.file)[j], (*str.file)[i]
}
