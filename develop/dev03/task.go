package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"sort"
	"strings"
)

/*
=== Утилита sort ===

Отсортировать строки (man sort)
Основное

Поддержать ключи

-k — указание колонки для сортировки
-n — сортировать по числовому значению
-r — сортировать в обратном порядке
-u — не выводить повторяющиеся строки

Дополнительное

Поддержать ключи

-M — сортировать по названию месяца
-b — игнорировать хвостовые пробелы
-c — проверять отсортированы ли данные
-h — сортировать по числовому значению с учётом суффиксов

Программа должна проходить все тесты. Код должен проходить проверки go vet и golint.
*/

type Config struct {
	file string
	key  uint
	num  bool
	rev  bool
	uniq bool
}

var config = Config{}

func initConfig() {
	flag.UintVar(&config.key, "k", 1, "Параметр для указания колонки по которой будет производиться сортировка. По умолчанию: 1")
	flag.BoolVar(&config.num, "n", false, "Параметр для сортировки только по числовому значению. По умолчанию выключен")
	flag.BoolVar(&config.rev, "r", false, "Параметр для сортировки в обратном порядке. По умолчанию выключен")
	flag.BoolVar(&config.uniq, "u", false, "Параметр для отсеивания повторяющихся элементов. По умолчанию выключен")

	flag.Usage = func() {
		fmt.Println("Программа для сортировки файла по столбцам по ключу.")
		fmt.Println("Синтаксис программы: sort [-f args]... [file]")
		flag.PrintDefaults()
	}

	flag.Parse()

	if !flag.Parsed() {
		flag.Usage()
		os.Exit(-1)
	}

	if len(flag.Args()) < 1 || len(flag.Args()) > 1 {
		flag.Usage()
		os.Exit(-1)
	}

	config.file = flag.Arg(flag.NArg() - 1)
}

func checkConfig() {
	if config.key < 1 {
		log.Panic("ключ должен быть больше 0")
	}
}

func main() {
	initConfig()
	checkConfig()

	file, err := os.Open(config.file)
	if err != nil {
		log.Panic(err)
	}
	defer file.Close()

	data := File{}

	scanner := bufio.NewScanner(file)

	if config.uniq {
		mapp := make(map[string]struct{})
		for scanner.Scan() {
			text := scanner.Text()
			if _, ok := mapp[text]; text != "" && !ok {
				mapp[text] = struct{}{}
				//TODO: Сделать без обрезки пробельных символов через regexp
				data = append(data, strings.Fields(text))
			}
		}
	} else {
		for scanner.Scan() {
			//TODO: Сделать без обрезки пробельных символов через regexp
			text := scanner.Text()
			if text != "" {
				data = append(data, strings.Fields(text))
			}
		}
	}

	if scanner.Err() != nil {
		log.Panic(err)
	}
	file.Close()

	fmt.Print(Exec(data, config))
}

func Exec(file File, conf Config) string {
	if !file.checkKey(conf.key) {
		conf.key = 1
	}

	switch {
	case conf.num:
		if conf.rev {
			sort.Sort(sort.Reverse(&NumSort{&file, conf.key}))
		} else {
			sort.Sort(&NumSort{&file, conf.key})
		}
	default:
		if conf.rev {
			sort.Sort(sort.Reverse(&StringSort{&file, conf.key}))
		} else {
			sort.Sort(&StringSort{&file, conf.key})
		}
	}

	return file.String()
}

type File [][]string

func (file *File) checkKey(key uint) bool {
	for i := range *file {
		if uint(len((*file)[i])) < key {
			return false
		}
	}

	return true
}

func (file *File) String() string {
	builder := strings.Builder{}
	for _, data := range *file {
		builder.WriteString(fmt.Sprintf("%v\n", strings.Join(data, "\t")))
	}
	return strings.Trim(builder.String(), "\n")
}
