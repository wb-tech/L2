package main

import (
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"sync"
)

var (
	port string
	host string
)

func initFlags() {
	if len(os.Args) < 2 {
		log.Fatal("Введите адрес для подключения")
	}

	host = os.Args[1]

	if len(os.Args) >= 2 {
		port = os.Args[2]
	}
}

func main() {
	initFlags()

	address := net.JoinHostPort(host, port)

	listener, err := net.Listen("tcp", address)
	if err != nil {
		log.Fatal(err)
	}
	defer listener.Close()

	conn, err := listener.Accept()
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	wg := sync.WaitGroup{}

	wg.Add(1)
	go func(wg *sync.WaitGroup) {
		defer wg.Done()
		for {
			var buf = make([]byte, 1000)
			if _, err = conn.Read(buf); err != nil {
				if err == io.EOF {
					return
				}

				fmt.Println(err)
				return
			}
			fmt.Println(string(buf))
		}
	}(&wg)

	wg.Wait()
	fmt.Println("Connection closed...")
}
