package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"sync"
	"time"
)

/*
=== Утилита telnet ===

Реализовать примитивный telnet клиент:
Примеры вызовов:
go-telnet --timeout=10s host port go-telnet mysite.ru 8080 go-telnet --timeout=3s 1.1.1.1 123

Программа должна подключаться к указанному хосту (ip или доменное имя) и порту по протоколу TCP.
После подключения STDIN программы должен записываться в сокет, а данные полученные и сокета должны выводиться в STDOUT
Опционально в программу можно передать таймаут на подключение к серверу (через аргумент --timeout, по умолчанию 10s).

При нажатии Ctrl+D программа должна закрывать сокет и завершаться. Если сокет закрывается со стороны сервера, программа должна также завершаться.
При подключении к несуществующему сервер, программа должна завершаться через timeout.
*/

var timeout string

var (
	host string = "localhost"
	port string = "22"
)

func initFlags() {
	flag.StringVar(&timeout, "timeout", "10s", "Время для подключения к серверу")
	flag.Usage = func() {
		fmt.Println("Программа для подключеиня к серверу и выполнения команд на сервере")
		flag.PrintDefaults()
	}
	flag.Parse()

	if flag.NArg() < 1 {
		log.Fatal("Введите адрес для подключения")
	}

	host = flag.Arg(0)

	if flag.NArg() >= 2 {
		port = flag.Arg(1)
	}
}

func main() {
	initFlags()

	address := net.JoinHostPort(host, port)

	dur, err := time.ParseDuration(timeout)
	if err != nil {
		log.Fatal(err)
	}

	var conn net.Conn

	start := time.Now()
	for time.Since(start) < dur {
		conn, err = net.Dial("tcp", address)
		if err == nil {
			break
		}

	}

	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	wg := sync.WaitGroup{}

	wg.Add(1)
	go func(wg *sync.WaitGroup) {
		defer wg.Done()
		scanner := bufio.NewScanner(os.Stdin)
		for scanner.Scan() {
			if _, err = conn.Write(scanner.Bytes()); err != nil {
				fmt.Println(err)
				return
			}
		}
	}(&wg)

	wg.Wait()
	fmt.Println("Connection closed...")
}
