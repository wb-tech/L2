package main

import "testing"

func TestAnagrammCheck(t *testing.T) {
	testData := []struct {
		name   string
		word1  string
		word2  string
		result bool
	}{
		{
			name:   "EmptyCheck",
			word1:  "",
			word2:  "",
			result: true,
		},
		{
			name:   "FirstLengthCheck",
			word1:  "абвгде",
			word2:  "абвг",
			result: false,
		},
		{
			name:   "SecondLengthCheck",
			word1:  "абвг",
			word2:  "абвгде",
			result: false,
		},
		{
			name:   "DuplicateCheck",
			word1:  "абввг",
			word2:  "абвгг",
			result: false,
		},
		{
			name:   "ReverseCheck",
			word1:  "абввг",
			word2:  "гввба",
			result: true,
		},
		{
			name:   "LengthCheck",
			word1:  "yИРРrSaУцkwNcDюкгxПнЗYaMrXЩЧuиRжsгучеeиЭоDаЖйktшэVКЬLукHжюАzLЛWIЩйШцЩжГкнTZуPshKgшЛAзьЮАiKЪтЕЧнгWЛЯVтKFrлАЩQГnWsPIХВЛoчVbЖEvЛSЦWzвхХzАuЙBваДеBsJлrКcсEюAХиВфaмБЖDвыEоsAфtДСbцRHIqЬЦIЗмРХXOKzЮdЪЩЪхзDPYюцЪmиFФIОqзdсшoсьRЙуЫЬDКRЪВЗOГйpуТSРЦнЪDТoдHШсxpюЧИtЖPVЭУЩЦзВшИфlfыRsЕОEЭWWЖСЩjIпжюДбЧхЩжфОУNmвUЬфEiХыуJFбaрЭaЯUiКFиMусnxРхфюGлдcкFТФNйNIаьNрBKpжйЖвЫЬяuБqxАPщIKaNнYЪPNХЬjкЮягAqЧЮuЪоUdЯЙЬСЛтюоКнEбmЩиФгмFcтИlнЪыhяzГЫхiЪВоPпDUАSЭiэXдAIЗPрsМЦSЭШPчQЭiNуЭЛУщЬUfxуЖОqтЭысгЦдjAUPОsOСДЛбьpЛМрHЩqмТUpоНЪmКrХгдgDюмДКwiEмaьTВкRадчЗjЮZfжrАюEoWKnМКAITнЭзпКСЯVъЧОXАфhЫkdRoKCPФЮbNГyZXнKоtOиФVgЩЪQNSЯнЦЫUiBWСpеЪCKРIьtNШвецГтглиЭШyЬxЬtаzgMktъnщичЩобЙьРСиHаСbббeчлSшbTэhэТЖЗлсШЭstПЭЪлАкьРэCMяlJzljАмГwtАЕмvюЯYCЧoчшkЪзxuХдйЭUIDvожЖмъCTjчДяwтJvVВцiццbSnVпTDЭЙУVмяLЕХBXnмqЯHЮйБstКЩbЯъxБJVэКХНОНМЙпФцРлcлэЧAУДFфЗЩGudlDBаQЫЖfЬЯзvOАЛAtzГЦсЙBЭlQЭPdyчDmвМIпИbOФъЗхTяЗЬяЖЛLkAЬбхЫggдlEZцJPШФXcДсbмьKuЯбщiюКunдsskLnьFиoHfЭъlУцшаhLъМxQЫEOoэцXwХсZzhмgЕXМТpОЛbhщюEoЧхдatМДnjЬvwtqNamzkхЫМVчЕыПЧoУAкЙQэuШШWпЮyЩEажМPЩWЕrlФjфлхbCщrыhmnАSV",
			word2:  "цюtйАШWeOцAPxXvгEоxuсфНфRKЯOЯбъщпnQЭаDЙкУuиsюуzМZДхmХSDdIЧсоцyЭlNgiOМэEXзРсЦЗhcrsQLFЮЪAРЩLSsМОбЯуSКsБRэГАIЛлЩCxлЦHkшаЫлиУЪвqpcjзъьРJаоKБQtмqIлzоAаnхAOИdыГуkюUsьИЫRjюЬAтIJEpеЛсВxшkЮэjNBУэЭГuЖhЪДabхОтУVщдсtшоЕrЮгEQJmжчLЛеИzЩЮимTдЩhZPTlEgимВАМrЧIЛtiHКгnЪйCjPЪтbaдWcаUХкaЧszЩхФШKMШАКUDXnЕPжXЭвfшvйтВuЮDoцtWnLЭчжяэЬdмЮяUIЛЧfхWЗnгВFrLiHнХГоЪdgiЬЦVlbЖКжШХiVУЯoеКuжuТмpUКwЙъСjasЛДШЖуЪжорVэVьнХдЕlЫoКZPNпFпчCМцlпTИЬДеqФЯОЫцgКAцяйяифSЩХAcЬЩчAЧаЪBOPwEsaцxЗтaВЖGйнyхrАЩYЭUьINЖZнHЫЧЗЭЛОвщШЩRнDпwwМХpсiмрНbbдАТтъЭtяФaXlгkЦKzЪАчЕЭNнЦFфСцэЖtдgъСIиlьzVкЖoVDНуNЯzjHNSмQLжFхRbЧsЬюьфлIчиЭсCWowUяагСгlFФТIkЖPЧсSуxXвюяpfмПСKИЛEВxГgkфпЯyЮСыhWЭьЗЭRUЮqЫзЩдъыРSюСmVЩBлmЗхЩЦnmYbVрывTьAbДяДГdмtьзЖъzЗqPOhAeчYiШZnйmPсbчBqЪЛoцЭvЪnqfшtoцjркФКЬzarМKюАJЪннsЛоПDWЪлшлBCEтДDyDАФвЧWУЩrЩАKЖMаЭифкщЭMсГHKMАдAКШFьВбbТжюмsэзбСюTvРБnEЛМsЕЬDЪЙpWЯРБЫаРЩчiМdPhшЬoUkuYqХАУQдЬЬrTйщЙoкKNюzФtТКЙпFХКбtSlумNfmhцыykЕчШкЙбVIлTPWЛюэЭKбxЭхиGиЫОРJуЙЙЫЧвDщХcCыXФOуiнjJДЩлXФЯОГPtEдЬowзжXАфvBSBЖDоТbxЪюХЯгVМHKNПaVtgфpEEбQЬзЗuыINРЦuЕхмRОбЗcvнhiЦУОкNЭIPиЬДмP",
			result: true,
		},
	}

	for _, test := range testData {
		t.Run(test.name, func(t *testing.T) {
			if test.result != checkAnagramm(test.word1, test.word2) {
				t.Error("Wrong result from func")
			}
		})
	}
}

func TestAnagrammSet(t *testing.T) {

}
