package main

import (
	"fmt"
	"sort"
	"strings"
)

/*
=== Поиск анаграмм по словарю ===

Напишите функцию поиска всех множеств анаграмм по словарю.
Например:
'пятак', 'пятка' и 'тяпка' - принадлежат одному множеству,
'листок', 'слиток' и 'столик' - другому.

Входные данные для функции: ссылка на массив - каждый элемент которого - слово на русском языке в кодировке utf8.
Выходные данные: Ссылка на мапу множеств анаграмм.
Ключ - первое встретившееся в словаре слово из множества
Значение - ссылка на массив, каждый элемент которого, слово из множества. Массив должен быть отсортирован по возрастанию.
Множества из одного элемента не должны попасть в результат.
Все слова должны быть приведены к нижнему регистру.
В результате каждое слово должно встречаться только один раз.

Программа должна проходить все тесты. Код должен проходить проверки go vet и golint.

"абырвалг", "рлавыбаг"
*/
//Test add
//[]string{"абырвалг", "рлавыбаг", "пятак", "пятка", "тяпка", "листок", "слиток", "столик", "рлаеыбаг"})
func main() {

	fmt.Println(AnagrammSet(&[]string{"абырвалг", "рлавыбаг", "рлавыбаг", "рлавыбаг", "пятак", "пятка", "ТЯПКА", "листок", "слиток", "столик", "рлаеыбаг"}))
}

func AnagrammSet(words *[]string) *map[string][]string {
	anagSet := make(map[string][]string)

	for i := range *words {
		strings.ToLower((*words)[i])
	}

	for _, v := range *words {
		new := true
		for key := range anagSet {
			if checkAnagramm(v, key) {
				if !contains(anagSet[key], v) {
					anagSet[key] = append(anagSet[key], v)
				}
				new = false
			}
		}

		if new {
			anagSet[v] = []string{v}
		}
	}

	for key, _ := range anagSet {
		sort.Slice(anagSet[key], func(i, j int) bool {
			if anagSet[key][i] < anagSet[key][j] {
				return true
			}
			return false
		})
	}

	dropEmpty(&anagSet)

	return &anagSet
}

func dropEmpty(set *map[string][]string) {
	for key, _ := range *set {
		if len((*set)[key]) == 1 {
			delete(*set, key)
		}
	}
}

func contains(words []string, val string) bool {
	for i := range words {
		if words[i] == val {
			return true
		}
	}
	return false
}

func checkAnagramm(str1, str2 string) bool {
	rns1 := []rune(str1)
	rns2 := []rune(str2)

	res1 := make(map[rune]int)
	res2 := make(map[rune]int)

	for _, k := range rns1 {
		if _, ok := res1[k]; !ok {
			res1[k] = 1
		} else {
			res1[k]++
		}
	}

	for _, k := range rns2 {
		if _, ok := res2[k]; !ok {
			res2[k] = 1
		} else {
			res2[k]++
		}
	}

	if len(res1) != len(res2) {
		return false
	}

	for key := range res1 {
		if _, ok := res2[key]; !ok {
			return false
		}
		if res2[key] != res1[key] {
			return false
		}
	}

	return true
}
