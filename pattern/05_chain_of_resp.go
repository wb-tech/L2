package pattern

import (
	"fmt"
	"math/rand/v2"
)

/*
	Реализовать паттерн «цепочка вызовов».
Объяснить применимость паттерна, его плюсы и минусы, а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/Chain-of-responsibility_pattern
*/

/*
	Паттерн цепь обязанностей - поведенческий паттерн, позволяющий избежать привязки запроса от отправителя к конкретному получателю,
		позволяя тем самым обработать запрос несколькими получателями

	Применимость:
	- Запрос может быть обработан более чем одним получателем и настоящий обработчик заранее неизвестен
	- Запрос должен быть отправлен к одному из объектов-получателей
	- Набор объектов для обработки задается динамически

	Плюсы:
	- Ослабление связанности между объектами. Отправителю и получателю запроса ничего не известно друг о друге
	- В цепочку можно добавлять новые типы объектов, которые реализуют общий интерфейс
	- Реализует принцип открытости/закрытости

	Минусы:
	- Запрос может остаться никем не обработанным
*/

type Handler interface {
	Bind(Handler)
	Handle(*Order)
}

type Order struct {
	Id      int
	Article string
	Count   int
	Address string
}

func NewOrder(article, address string, count int) *Order {
	return &Order{
		rand.IntN(1000),
		article,
		count,
		address,
	}
}

type Storage struct {
	ProductArticle string
	CountProduct   int
	Next           Handler
}

func NewStorage(article string, count int) *Storage {
	return &Storage{
		article,
		count,
		nil,
	}
}

func (store *Storage) Bind(handler Handler) {
	store.Next = handler
}

func (store *Storage) Handle(order *Order) {
	if store.ProductArticle == order.Article {
		if store.CountProduct >= order.Count {
			fmt.Printf("На складе есть весь товар с артиклем %v. Товар будет отправлен по адресу %v в кол-ве %v\n", store.ProductArticle, order.Address, order.Count)
			store.CountProduct -= order.Count
			order.Count = 0
		} else {
			fmt.Printf("На скаледе с таким артиклем есть %v товаров из %v. Эта часть товаров будет отправлена по адресу %v\n", store.CountProduct, order.Count, order.Address)
			order.Count -= store.CountProduct
			store.CountProduct = 0
			if store.Next != nil {
				store.Next.Handle(order)
			}
		}
	} else {
		fmt.Printf("Нет продукты с таким артиклем на складе %v\n", order.Article)
		if store.Next != nil {
			store.Next.Handle(order)
		}
	}
}

func ExampleChain() {
	order := NewOrder("CD", "Ленина, 8 ", 5)
	h1 := NewStorage("Flash", 10)

	h2 := NewStorage("Fork", 30)
	h2.Bind(h1)

	h3 := NewStorage("CD", 3)
	h3.Bind(h2)

	h4 := NewStorage("Plate", 25)
	h4.Bind(h3)

	h5 := NewStorage("CD", 4)
	h5.Bind(h4)

	h5.Handle(order)
}
