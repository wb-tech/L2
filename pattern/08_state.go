package pattern

import "fmt"

/*
	Реализовать паттерн «состояние».
Объяснить применимость паттерна, его плюсы и минусы, а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/State_pattern
*/

/*
	Паттерн состояние - поведенческий паттерн, изменяющий поведение объекта в зависимости от состояния.

	Применимость:
	- Поведение объекта зависист от состояния и должно изменяться во время выполненя
	- Когда в коде встречаетсяс моножество условных операторов, результат которых зависит состояния

	Плюсы:
	- Избавляет от множества больших условных операторов машины состояний
	- Концентрирует в одном месте код, связанный с определённым состоянием
	- Упрощает код контекста

	Минусы:
	- Может неоправданно усложнить код, если состояний мало и они редко меняются
*/

type MobileAlertStater interface {
	Alert() string
}

type MobileAlert struct {
	state MobileAlertStater
}

func (a *MobileAlert) Alert() string {
	return a.state.Alert()
}

func (a *MobileAlert) SetState(state MobileAlertStater) {
	a.state = state
}

func NewMobileAlert() *MobileAlert {
	return &MobileAlert{state: &MobileAlertVibration{}}
}

type MobileAlertVibration struct {
}

func (a *MobileAlertVibration) Alert() string {
	return "Vrrr... Brrr... Vrrr..."
}

type MobileAlertSong struct {
}

func (a *MobileAlertSong) Alert() string {
	return "Белые розы, Белые розы. Беззащитны шипы..."
}

func ExampleState() {
	state := &MobileAlert{}
	if true {
		state.SetState(&MobileAlertVibration{})
	} else {
		state.SetState(&MobileAlertSong{})
	}

	fmt.Println(state.Alert())
}
