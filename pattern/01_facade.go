package pattern

import "fmt"

/*
	Реализовать паттерн «фасад».
Объяснить применимость паттерна, его плюсы и минусы,а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/Facade_pattern
*/

/*
	Паттерн Фасад - структурынй паттерн проектирования, который предоставляет унифицированный высокоуровневый интерфейс для взаимодействия с подсистемой.

	Применимость:
	- Предоставление простого интерфейса к сложной системе
	- Многочисленные зависимости между клиентами и классами реализации
	- Требуется разложить подсистему на отдельные уровни

	Плюсы:
	- Ослабление связности между клиентами и подсистемами
	- Изоляция клиентов от компонентов подсистемы
	- Уменьшение числа зависимостей
	- Не препятствует прямому общению между приложением и классам подсистемы

	Минусы:
	- Возможно увеличение сложности фасада, что может привести к увеличению связей между компонентами
	- Возможна излишняя простота
	- Снижение гибкости
*/

type Facade struct {
	swit      Switcher
	chanelSet ChanSetter
	sourceSet SourceSetter
}

func (fac *Facade) On() {
	fac.swit.On()
}
func (fac *Facade) Off() {
	fac.swit.Off()
}
func (fac *Facade) SetSource(source string) {
	fac.sourceSet.SetSource(source)
}
func (fac *Facade) SetChannel(chanel string) {
	fac.chanelSet.SetChan(chanel)
}

type Switcher interface {
	On()
	Off()
}

type Switch struct{}

func (swi *Switch) On() {
	fmt.Println("On...")
}

func (swi *Switch) Off() {
	fmt.Println("Off...")
}

type ChanSetter interface {
	SetChan(chanel string)
}

type Chanel struct{}

func (ch *Chanel) SetChan(chanel string) {
	switch chanel {
	case "1":
		fmt.Println(1)
	case "2":
		fmt.Println(2)
	default:
		fmt.Println("Uncnown chanel")
	}
}

type SourceSetter interface {
	SetSource(source string)
}

type Source struct{}

func (src *Source) SetSource(source string) {
	fmt.Printf("Source set: %v\n", source)
}
