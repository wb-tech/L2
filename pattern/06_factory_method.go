package pattern

import "fmt"

/*
	Реализовать паттерн «фабричный метод».
Объяснить применимость паттерна, его плюсы и минусы, а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/Factory_method_pattern
*/

/*
	Паттерн фабричный метод - порождающий паттерн, открывающий интерфейс для создания объекта, но при этом решение о создании класса остается за подклассами.

	Применимость:
	- Заранее неизвестно, какой объект необходимо создать
	- Класс спроектирован так, чтобы создоваемые объекты определялись подклассами
	- Класс делегирует свои обязанности по созданию подклассу и надо локализовать информацию о том, кокой класс берет на себя обязанность создания

	Плюсы:
	- Избавляет главный класс от привязки к конкретным типам объектов
	- Упрощает добавление новых типов объектов в программу
	- Выделяет код производства объектов в одно место, упрощая поддержку кода

	Минусы:
	- Может привести к созданию больших параллельных иерархий классов, так как для каждого типа объекта надо создать свой подкласс создателя
*/

const (
	Pepperoni = iota
	FourSeason
	Caprichosa
	FourChees
)

type PizzaCreator interface {
	CreatePizza(PizzaType int) Pizza
}

type NeapolitanianPizzaCreator struct{}

func (neap *NeapolitanianPizzaCreator) CreatePizza(PizzaType int) Pizza {
	switch PizzaType {
	case 0:
		return &NeapolitanianPepperoniPizza{}
	case 1:
		return &NeapolitanianFourSeasonPizza{}
	case 2:
		return &NeapolitanianCaprichosaPizza{}
	case 3:
		return &NeapolitanianFourCheesPizza{}
	default:
		return nil
	}
}

type Pizza interface {
	PrintInfo()
}

type NeapolitanianPepperoniPizza struct{}

func (pep *NeapolitanianPepperoniPizza) PrintInfo() {
	fmt.Println("NeapolitanianPepperoniPizza")
}

type NeapolitanianFourSeasonPizza struct{}

func (pep *NeapolitanianFourSeasonPizza) PrintInfo() {
	fmt.Println("NeapolitanianFourSeasonPizza")
}

type NeapolitanianCaprichosaPizza struct{}

func (pep *NeapolitanianCaprichosaPizza) PrintInfo() {
	fmt.Println("NeapolitanianCaprichosaPizza")
}

type NeapolitanianFourCheesPizza struct{}

func (pep *NeapolitanianFourCheesPizza) PrintInfo() {
	fmt.Println("NeapolitanianFourCheesPizza")
}

func ExamplefabricMethod() {
	NeapCreator := &NeapolitanianPizzaCreator{}
	pizza := NeapCreator.CreatePizza(Caprichosa)
	pizza.PrintInfo()
}
