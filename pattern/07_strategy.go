package pattern

import (
	"fmt"
	"strconv"
	"strings"
)

/*
	Реализовать паттерн «стратегия».
Объяснить применимость паттерна, его плюсы и минусы, а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/Strategy_pattern
*/

/*
	Паттерн стратегия - поведенческий паттерн, определяющий семейство алгоритмов, инкапсулирующий их и делающий их взаимо заменяемыми.

	Применимость:
	- Наличие родственнных классов, отличающихся только поведением
	- Наличие нескольких разновидностей алгоритмов
	- В классе определено много вариантов поведения и представленныз разветвленными условными конструкциями

	Плюсы:
	- Вызов всех алгоритмов одним стандартным образом
	- Отказ от использования переключателей и/или условных операторов
	- Инкапсуляция реализации различных алгоритмов, система становится независимой от возможных изменений бизнес-правил

	Минусы:
	- Создание дополнительных классов
*/

type Compositor interface {
	Swap(i, j int)
}

type NumSort struct {
	file *[][]string
	key  uint
}

func (num *NumSort) Less(i, j int) bool {
	numI, err := strconv.ParseFloat((*num.file)[i][num.key-1], 64)
	if err != nil {
		return false
	}

	numJ, err := strconv.ParseFloat((*num.file)[j][num.key-1], 64)
	if err != nil {
		return true
	}

	if numI >= numJ {
		return false
	}

	return true
}

func (num *NumSort) Len() int {
	return len(*num.file)
}

func (num *NumSort) Swap(i, j int) {
	(*num.file)[i], (*num.file)[j] = (*num.file)[j], (*num.file)[i]
}

type StringSort struct {
	file *[][]string
	key  uint
}

func (str *StringSort) Less(i, j int) bool {
	if strings.Compare((*str.file)[i][str.key-1], (*str.file)[j][str.key-1]) != -1 {
		return false
	}

	return true
}

func (str *StringSort) Len() int {
	return len(*str.file)
}

func (str *StringSort) Swap(i, j int) {
	(*str.file)[i], (*str.file)[j] = (*str.file)[j], (*str.file)[i]
}

func ExampleStrategy() {
	var comp Compositor
	key := uint(2)
	file := [][]string{
		{"A", "B", "C"},
		{"A", "C", "B"},
		{"B", "A", "C"},
		{"B", "C", "A"},
		{"C", "A", "B"},
		{"C", "B", "A"},
	}

	switch "string" {
	case "num":
		comp = &NumSort{file: &file, key: key}
	case "string":
		comp = &StringSort{file: &file, key: key}
	}

	comp.Swap(0, 5)

	fmt.Println(file)
}
