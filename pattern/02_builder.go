package pattern

import "fmt"

/*
	Реализовать паттерн «строитель».
Объяснить применимость паттерна, его плюсы и минусы, а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/Builder_pattern
*/

/*
	Паттерн строитель -  порождающий паттерн, который "строит" сложные объекты, отделяя их от представления самого объекта

	Применимость:
	- Создаение объекста не должно зависеть от частей самого объекста
	- Процесс конструирования должен обеспечивать различные представления конструируемого объекта
	- Требуется разложить подсистему на отдельные уровни

	Плюсы:
	- Упрощение процесса создания сложных объектов
	- Сокрытие сложной логики  создания оюъекта
	- Разделение процесса создания и контроля за объектом

	Минусы:
	- Усложнение кода из-за доп иерархий
	- Для конструирования простых объектов не эффективен
*/

type House struct {
	Floors            int
	Walls             int
	FundamentMaterial string
	WallMaterial      string
}

type HouseBuilder interface {
	SetFundament()
	SetWalls()
	SetFloors()
	GetResult() House
}

type IglooBuilder struct {
	Hs House
}

func (ig *IglooBuilder) SetFundament() {
	ig.Hs.FundamentMaterial = "snow"
}

func (ig *IglooBuilder) SetWalls() {
	ig.Hs.WallMaterial = "ice"
	ig.Hs.Walls = 1
}

func (ig *IglooBuilder) SetFloors() {
	ig.Hs.Floors = 1
}

func (ig *IglooBuilder) GetResult() House {
	return ig.Hs
}

type OneFloorHouseBuilder struct {
	Hs House
}

func (ofhb *OneFloorHouseBuilder) SetFundament() {
	ofhb.Hs.FundamentMaterial = "cement"
}

func (ofhb *OneFloorHouseBuilder) SetWalls() {
	ofhb.Hs.WallMaterial = "stone"
	ofhb.Hs.Walls = 4
}

func (ofhb *OneFloorHouseBuilder) SetFloors() {
	ofhb.Hs.Floors = 10
}

func (ofhb *OneFloorHouseBuilder) GetResult() House {
	return ofhb.Hs
}

type Prorab struct {
}

func (prorab *Prorab) Consstruct(houseBuilder HouseBuilder) {
	houseBuilder.SetFundament()
	houseBuilder.SetWalls()
	houseBuilder.SetFloors()
}

func Example() {
	prorab := Prorab{}
	igloo := &IglooBuilder{}
	prorab.Consstruct(igloo)
	fmt.Println(igloo.GetResult())

	onefloor := &OneFloorHouseBuilder{}
	prorab.Consstruct(onefloor)
	fmt.Println(onefloor.GetResult())
}
