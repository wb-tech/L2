package pattern

import "fmt"

/*
	Реализовать паттерн «комманда».
Объяснить применимость паттерна, его плюсы и минусы, а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/Command_pattern
*/

/*
	Паттерн команда - поведенческий паттерн, инкапсулирующий запрос в объект,
		позволяя тем самым параметрезировать клиенты для разных запросов, ставить запросы в очередь и отменять сами запросы.

	Применимость:
	- Параметризация объектов выполняемым действием
	- Поддержка отмены действий
	- Определение, постановка в очередь, выполнение в разное время
	- Структурирование системы на основе высокоуровневых операций

	Плюсы:
	- Уменьшение зависимости между объектами: объекты-инициаторы не привязаны напрямую к объектам, которые выполняют операции.
	- Унификация обработки событий или запросов к системе.
	- Возможность формирования очереди команд.
	- Возмжность отмены действий.
	- Возможность сохранения состояние.

	Минусы:
	- По умолчанию команда выполняется только над одним объектом.
	- Требуется изначально знать, кто может обработать команду и какое конкретное действие должно быть совершено.
	- Увеличение количества и усложнение структуры кода за счёт появления новых иерархий классов.
*/

type Car struct {
	speed int
}

func (car *Car) Move() {
	fmt.Printf("Car move with speed %v\n", car.speed)
}

func (car *Car) Stop() {
	fmt.Println("Car stopped")
}

type Command interface {
	Execute()
}

type CarMoveCommand struct {
	Car *Car
}

func NewMoveCommand(car *Car) *CarMoveCommand {
	return &CarMoveCommand{Car: car}
}

func (carMoveCommand *CarMoveCommand) Execute() {
	carMoveCommand.Car.Move()
}

type CarStopCommand struct {
	Car *Car
}

func NewStopCommand(car *Car) *CarStopCommand {
	return &CarStopCommand{Car: car}
}

func (carStopCommand *CarStopCommand) Execute() {
	carStopCommand.Car.Stop()
}

type CarMoveInvoker struct {
	command Command
}

func (carMoveInvoker *CarMoveInvoker) SetCommand(command Command) {
	carMoveInvoker.command = command
}

func (carMoveInvoker *CarMoveInvoker) Run() {
	carMoveInvoker.command.Execute()
}

func ExampleCommand() {
	car := &Car{100}
	invoker := CarMoveInvoker{}

	carMove := NewMoveCommand(car)
	invoker.SetCommand(carMove)
	invoker.Run()

	carStop := NewStopCommand(car)
	invoker.SetCommand(carStop)
	invoker.Run()
}
